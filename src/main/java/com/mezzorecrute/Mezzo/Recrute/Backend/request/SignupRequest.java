package com.mezzorecrute.Mezzo.Recrute.Backend.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SignupRequest {

    private String username;
    private String email;
    private String password;
    private  String adresse;
    private String date_naiss;
    private Long phone;
    private Set<String> roles;


}
