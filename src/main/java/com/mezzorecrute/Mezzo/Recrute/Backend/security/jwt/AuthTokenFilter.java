package com.mezzorecrute.Mezzo.Recrute.Backend.security.jwt;

import com.mezzorecrute.Mezzo.Recrute.Backend.Utilisateur.UtilisateurDetailsServiceImpl;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
// ay request tsir tet3ada par les filter ta3 spring
public class AuthTokenFilter  extends OncePerRequestFilter {
    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private UtilisateurDetailsServiceImpl utilisateurDetailsService;

    private static final Logger logger = LoggerFactory
            .getLogger(AuthTokenFilter.class);
    //    bch nchoufou request eli saret par un utilisateur qui a un compte  ou non
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain)
            throws ServletException, IOException {
        try {
//recupere jwt te3na si null y9olk mahouch connecté ma3andekch token matnajemch taaml l action  heki
            String jwt = parseJwt(request);
            if (jwt != null && jwtUtils.validateJwtToken(jwt)) {

                String email = jwtUtils
                        .getEmailFromJwtToken(jwt);

                UserDetails utilisateurDetails = utilisateurDetailsService
                        .loadUserByUsername(email);

                UsernamePasswordAuthenticationToken authentication =
                        new UsernamePasswordAuthenticationToken(
                                utilisateurDetails, null,
                                utilisateurDetails.getAuthorities());

                authentication.setDetails(new WebAuthenticationDetailsSource()
                        .buildDetails(request));

                SecurityContextHolder.getContext()
                        .setAuthentication(authentication);
            }
        } catch (Exception e) {
            logger.error("Cannot set employee authentication: {}", e);
        }

        filterChain.doFilter(request, response);

    }
    private String parseJwt(HttpServletRequest request) {
        String headerAuth = request.getHeader("Authorization");
// JWT Token is in the form "Bearer token". Remove Bearer word and get only the Token
        if (StringUtils.hasText(headerAuth)
                && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7, headerAuth.length());
        }

        return null;
    }
}
