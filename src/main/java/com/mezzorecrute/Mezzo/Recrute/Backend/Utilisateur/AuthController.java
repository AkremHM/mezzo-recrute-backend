package com.mezzorecrute.Mezzo.Recrute.Backend.Utilisateur;

import com.mezzorecrute.Mezzo.Recrute.Backend.request.LoginRequest;
import com.mezzorecrute.Mezzo.Recrute.Backend.request.SignupRequest;
import com.mezzorecrute.Mezzo.Recrute.Backend.response.JwtResponse;
import com.mezzorecrute.Mezzo.Recrute.Backend.response.MessageResponse;
import com.mezzorecrute.Mezzo.Recrute.Backend.role.ERole;
import com.mezzorecrute.Mezzo.Recrute.Backend.role.Role;
import com.mezzorecrute.Mezzo.Recrute.Backend.role.RoleRepository;
import com.mezzorecrute.Mezzo.Recrute.Backend.security.jwt.JwtUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins ="http://localhost:4200", maxAge = 3600, allowCredentials="true")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UtilisateurRepository utilisateurRepository;

    @Autowired
    RoleRepository roleRepository;

//    @Autowired
//    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Validated @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()

                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtUtils.generateJwtToken(authentication);

        UtilisateurDetailsImpl userDetails = (UtilisateurDetailsImpl)
                authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities()
                .stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(
                jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                userDetails.getPassword(),
                userDetails.getAdresse(),
                userDetails.getPhone(),
                userDetails.getDate_naiss(),

                roles));
    }
    @PostMapping("/registre")
    public ResponseEntity<?> registerUser
            (@RequestBody SignupRequest signUpRequest) {


//1--> ken l email mesta3ml y5arejlna message  error
        if (utilisateurRepository
                .existsByEmail(signUpRequest.getEmail())) {

            return ResponseEntity.badRequest()
                    .body(new MessageResponse
                            ("Error: Email deja utilisé!"));
        }

        // Create new user account
        Utilisateur utilisateur= new Utilisateur(
                signUpRequest.getUsername(),
                signUpRequest.getEmail(),
//                2-->password yetsajel mcody
               signUpRequest.getPassword(),
                signUpRequest.getAdresse(),
                signUpRequest.getPhone(),
                signUpRequest.getDate_naiss());
        Set<String> strRoles = signUpRequest.getRoles();
        Set<Role> roles = new HashSet<>();
//3--> hne tasty role
        if (strRoles == null) {
            Role utilisateurRole = roleRepository
                    .findByName(ERole.ROLE_CANDIDAT)
                    .orElseThrow(() -> new RuntimeException
                            ("Error: Role is not found."));
            roles.add(utilisateurRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository
                                .findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException
                                        ("Error: Role admin is not found."));
                        roles.add(adminRole);

                        break;
                    case "responsablededeparment":
                        Role responsablededeparment = roleRepository
                                .findByName(ERole.ROLE_RD)
                                .orElseThrow(() -> new RuntimeException
                                        ("Error: Role responsable de deparmentis not found."));
                        roles.add(responsablededeparment);

                        break;
                    case "responsablerecrutment":
                        Role responsablerecrutmentRole = roleRepository
                                .findByName(ERole.ROLE_RC)
                                .orElseThrow(() -> new RuntimeException
                                        ("Error: Role responsable recrutment is not found."));
                        roles.add(responsablerecrutmentRole);

                        break;
                    case "recruteur":
                        Role recruteurRole = roleRepository
                                .findByName(ERole.ROLE_RECRUTEUR)
                                .orElseThrow(() -> new RuntimeException
                                        ("Error: Role recruteur is not found."));
                        roles.add(recruteurRole);

                        break;
                    case "employee":
                        Role employeeRole = roleRepository
                                .findByName(ERole.ROLE_RECRUTEUR)
                                .orElseThrow(() -> new RuntimeException
                                        ("Error: Role employee is not found."));
                        roles.add(employeeRole);

                        break;
                    default:
                        Role defaultRole = roleRepository
                                .findByName(ERole.ROLE_CANDIDAT)
                                .orElseThrow(() -> new RuntimeException
                                        ("Error: Role candidat is not found."));
                        roles.add(defaultRole);
                }
            });
        }

        utilisateur.setRoles(roles);
        utilisateurRepository.save(utilisateur);

        return ResponseEntity.ok(new MessageResponse
                ("Utilisateur registered successfully!"));
    }

}
