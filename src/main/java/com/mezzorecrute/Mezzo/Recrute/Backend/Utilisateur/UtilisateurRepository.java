package com.mezzorecrute.Mezzo.Recrute.Backend.Utilisateur;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface UtilisateurRepository
        extends MongoRepository<Utilisateur, String> {

    Optional<Utilisateur> findByUsername(String username);
    Optional<Utilisateur> findByEmail(String email);
    Boolean existsByEmail(String email);
}
