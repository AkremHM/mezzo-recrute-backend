package com.mezzorecrute.Mezzo.Recrute.Backend.Utilisateur;

import com.mezzorecrute.Mezzo.Recrute.Backend.role.Role;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Document(collection = "utilisateur")
@Data
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Utilisateur {
    @Id
    private String id;
    private String username;
    // the email is un inique and i add haja f application.yml that make it work
    @Indexed(unique = true)
    private String email;
    private String password;
    private String adresse;
    private String gender;
    private Long phone;
    private String date_naiss;
    private String cin;
    private String cv;
    private String image;
    @DBRef
    private Set<Role> roles = new HashSet<>();
    public Utilisateur (){}
    public Utilisateur(String username, String email, String password,
                       String adresse , Long phone , String date_naiss ) {

        this.username = username;
        this.email = email;
        this.password = password;
        this.adresse =adresse;
        this.phone = phone;
        this.date_naiss = date_naiss;
    }
}
